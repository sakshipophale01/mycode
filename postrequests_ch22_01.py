#Using http POST to validate JSON
import requests

url= "http://validate.jsontest.com/"

def main():
    mydata = {"json": "{'fruit': ['apple', 'pear'], 'vegetable': ['carrot']}"}

    resp = requests.post(url,data=mydata)

    respJson = resp.json()

    print(respJson)

    # JUST display the value of "validate"
    print(f"Is your JSON valid? {respJson['validate']}")


if __name__ == "__main__":
    main()
