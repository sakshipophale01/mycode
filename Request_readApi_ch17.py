import requests
import pprint

url = "https://www.anapioficeandfire.com/api"

def main():
    resp = requests.get(url)

    allData = resp.json()
    print("Following are the available API's")
    pprint.pprint(allData)

    print("---Displaying books details---")
    getBooks = requests.get(url+"/books")
    books = getBooks.json()

    for singlebook in books:
        print(f"{singlebook['name']}, pages - {singlebook['numberOfPages']}")
        print(f"\tAPI URL -> {singlebook['url']}\n")
        # print ISBN
        print(f"\tISBN -> {singlebook['isbn']}\n")
        print(f"\tPUBLISHER -> {singlebook['publisher']}\n")
        print(f"\tNo. of CHARACTERS -> {len(singlebook['characters'])}\n")

if __name__ == "__main__":
    main()
