import requests

TIMEURL = "http://date.jsontest.com"
IPURL = "http://ip.jsontest.com"
VALIDURL = "http://validate.jsontest.com/"

def main():
    time = requests.get(TIMEURL)
    currentTime = time.json()

    ip = requests.get(IPURL)
    currentIp = ip.json()

    with open("server.txt") as myFile:
        myServer = myFile.readlines();

    jsonToTest ={}
    jsonToTest['time'] = currentTime
    jsonToTest['ip'] = currentIp
    jsonToTest['server'] = myServer

    finalData = {}
    finalData['json']=str(jsonToTest)

    resp = requests.post(VALIDURL, data=finalData)
    respJson = resp.json()

    print(f"Is your JSON valid? {respJson['validate']}")

if __name__ == "__main__":
    main()
