import requests

url="http://pokeapi.co/api/v2/pokemon/"

def main():
    healWords =[]
    pokemon = requests.get(url+"?limit=1000")
    pokemonJson = pokemon.json()

    print("Get pokemon name having word heal")
    for poke in pokemonJson["results"]:
        if 'heal' in poke['name']:
            healWords.append(poke['name'])

    print(f"There are {len(healWords)} words that contain the word 'heal' in the Pokemon Item API!")
    print("List of Pokemon items containing heal: ")
    print(healWords)

if __name__ == "__main__":
    main()
