import requests
from pprint import pprint


def main():
    URL = "https://swapi.dev/api/people/4"
    resp = requests.get(URL)
    if resp.status_code == 200:
        print('Displaying JSON Data')
        print(pprint(resp.json()))
        finalResp = resp.json()
        print("\nDisplay data in challenge format\n")
        #using fstring
        print(f"{finalResp['name']} was born in the year {finalResp['birth_year']}. His eyes are now {finalResp['eye_color']} and his hair color is {finalResp['hair_color']}.")
    else:
        print('Something went wrong!! Sorry')

if __name__ == '__main__':
    main()
