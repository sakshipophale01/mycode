#when you import urllib, u need to import json

import urllib.request
import json

MAJORTOM = "http://api.open-notify.org/astros.json"

def main():
    groundUrl = urllib.request.urlopen(MAJORTOM)

    helmet = groundUrl.read()

    print("Uncoming Hemlet: ", helmet)

    helmetJson = json.loads(helmet.decode("utf-8"))

    print('Json helmet type: ', type(helmetJson))

    # this returns a LIST of the people on this ISS
    print(helmetJson["people"])

    # list the FIRST astro in the list
    print(helmetJson["people"][0])

    # list the SECOND astro in the list
    print(helmetJson["people"][1])

    # list the LAST astro in the list
    print(helmetJson["people"][-1])

    print("\nPrinting people from json\n")
    for astro in helmetJson["people"]:
        print(astro)

    print("\nPrinting people's name from json\n")
    for astro in helmetJson["people"]:
        print(astro["name"])

    #challenge :
    print("Displaying challenge data")
    print("------------------------------")
    print("People in space: ", len(helmetJson["people"]))
    for astro in helmetJson["people"]:
        print(astro["name"]," on the craft ", astro["craft"])



if __name__ == "__main__":
    main()



