import requests
import pprint

#This is single threaded which might take some time when we Digin the data.. will see multithreaded solution soon
url = "https://www.anapioficeandfire.com/api"

def main():
    print("---GoT Challenge---")

    userIn = input("Enter any number from 1-1000")
    getCharacters = requests.get(url + f"/characters/{userIn}")
    character = getCharacters.json()
    print('Received Character, follwing are the allengiances')
    print(character['allegiances'])
    print(f"\nName: {character['name']}")
    print("--------------------------------------------------")
    if character['allegiances']:
        print(f"Returning the house(s) affilated by {character['name']}")
        for h in character['allegiances']:
            house = requests.get(h)
            houseJson = house.json()
            print("-", houseJson['name'])
    print("--------------------------------------------------")
    if character['books']:
        print(f"Returning the books affiliated by {character['name']}")
        for b in character['books']:
            book = requests.get(b)
            bookJson = book.json()
            print("-", bookJson['name'])


if __name__ == "__main__":
    main()

