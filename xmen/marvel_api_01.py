#Make your Marvel dev account. Head on over to https://developer.marvel.com/
#Get Key: Public and private
#to call marvel api we need to pass md5 hash value of(random_timestamp+privatekey+pubkey)
#For example, a user with a public key of "1234" and a private key of "abcd" could construct
# a valid call as follows: http://gateway.marvel.com/v1/public/comics?ts=1&apikey=1234&hash=ffd275c5130566a2916217b101f26150
#The hash value is the md5 digest of 1abcd1234

'''Run the prg on belw terminal by passing cmd
# cd PubPriKeyApiCalls
# python callMarvelApi.py --priv marvel.priv --pub marvel.pub --hero Archangel
'''

import requests
import argparse
import hashlib
import time
import pprint

API = 'http://gateway.marvel.com/v1/public/characters'

def genHash(rand,privateKey,publicKey):
    return hashlib.md5((f"{rand}{privateKey}{publicKey}").encode("utf-8")).hexdigest() # create an MD5 hash


def marvelCallApi(rand, mdHash, publicKey, hero):
    r =requests.get(f"{API}?name={hero}&ts={rand}&apikey={publicKey}&hash={mdHash}") #marvelApi will need these params
    if r.status_code != 200:
        response = None  #
    else:
        response = r.json()

    return response

def main():
    with open(args.priv) as privK:
        privateKey = privK.read().rstrip("\n") #rm blank spaces/lines

    with open(args.pub) as pubK:
        publicKey = pubK.read().rstrip("\n")

    rand = str(time.time()).rstrip(".") #generate random timestamp for MD5 hash val

    mdHash = genHash(rand,privateKey,publicKey)

    result = marvelCallApi(rand, mdHash, publicKey, args.hero)
    pprint.pprint(result)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--priv', help ='give path of private key file')
    parser.add_argument('--pub', help='give path of public key file')
    parser.add_argument('--hero', help='Pass any hero name eg: punisher, Archangel')

    args = parser.parse_args()
    main()


