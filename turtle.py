#The ISS is a satelite: orbits the Earth every hour and a half. Wow.
#That's fast. To tell where it is at any given moment, visit this link: http://api.open-notify.org/iss-now.json

'''more about turtle: https://docs.python.org/3.3/library/turtle.html?highlight=turtle#module-turtle'''

import requests
import turtle

url = 'http://api.open-notify.org/iss-now.json'
def main():
    resp = requests.get(url)
    respJson = resp.json()

    print("Converted Python data")
    print(respJson)
    input('\nISS data retrieved & converted. Press any key to continue')

    location = respJson['iss_position']
    lat = location['latitude']
    lon = location['longitude']
    print('\nLatitude: ', lat)
    print('Longitude: ', lon)

    # set screen size to 720 x 360 to match background image
    screen = turtle.Screen()
    screen.setup(720, 360)  # setup window size

    # to send turtle to particular lat long
    screen.setworldcoordinates(-180, -90, 180, 90)

    # add background map img
    screen.bgpic('iss_map.gif')

    ## My location : to our city location on map
    yellowlat = 47.6
    yellowlon = -122.3
    mylocation = turtle.Turtle()
    mylocation.penup()
    mylocation.color('yellow')
    mylocation.goto(yellowlon, yellowlat)
    mylocation.dot(15)
    mylocation.hideturtle()

    # now put ISS Satelite img on map bg
    screen.register_shape('spriteiss.gif')
    iss = turtle.Turtle()
    iss.shape('spriteiss.gif')
    iss.setheading(90)

    # now move iss to correct location
    lon = round(float(lon))
    lat = round(float(lat))
    iss.penup()
    iss.goto(lon, lat)
    turtle.mainloop()


if __name__ == "__main__":
    main()
